package main

import (
	"testing"
)

func TestNormalizeURL(t *testing.T) {
	testdata := []struct {
		input    string
		expected string
	}{
		// Case normalization
		{"Http://%3c%3C:%3c%3C@ExAmPlE.COM/", "http://%3C%3C:%3C%3C@example.com/"},
		// TODO: {"http://example.com/%3c%3C", "http://example.com/%3C%3C"},
		// Port normalization (omit default port)
		{"http://example.com:80/", "http://example.com/"},
		{"http://example.com:8080/", "http://example.com:8080/"},
		{"https://example.com:443/", "https://example.com/"},
		{"https://example.com:8443/", "https://example.com:8443/"},
		// No change
		{"http://example.com/?", "http://example.com/?"},
		{"http://example.com/path?query=string#fragment", "http://example.com/path?query=string#fragment"},
		{"http://example.com/path/?query=string#fragment", "http://example.com/path/?query=string#fragment"},
	}
	for _, d := range testdata {
		u, err := normalizeURL(d.input)
		if err != nil {
			t.Error(err)
			continue
		}
		if g, e := u, d.expected; g != e {
			t.Errorf("%q: normalized = %q, want = %q", d.input, g, e)
		}
	}

	testdata2 := []string{
		"",
	}
	for _, d := range testdata2 {
		u, err := normalizeURL(d)
		if err == nil {
			t.Errorf("%q: expected error, got %q", d, u)
		}
	}
}

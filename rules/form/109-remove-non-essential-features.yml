id: 109
bookId: 1
title: Remove non-essential features
slug: remove-non-essential-features
difficulty: hard
environmentalImpact: high
summary: | 
    Several studies (e.g. Cast Software and Standish Group) show that 70% of features requested by users are not essential, and that 45% are never used.  
    An application's initial development costs, technical debt and environmental impact can be minimized by reducing the range and depth of its features. By doing so, you also reduce the mechanical infrastructure necessary to run it. Furthermore, in terms of ergonomics, the fewer features the application has, the easier it will be to use. You must therefore concentrate on the user's primary needs in order to minimize the application's range of features.  
    If the application has already been developed, you must measure feature usage rates and, if the architecture allows it, disable, uninstall or delete the unused features.  
example: |
    The internet's latest success stories — e.g. Google, Twitter, WhatsApp, Pinterest and Instagram — provide a single service with basic functionality.  
    ![Google](https://framapic.org/VQthxYMChS8r/OFf6dSJhEMWv.png )
    ![Yahoo](https://framapic.org/uThGOI6JlVJh/hLggC7jFQFV3.png)  
    *Google's interface is much more basic than that of Yahoo!, and thus twice as light to download.*
bookContext: Design
application: Features
priority: 0
context: Design

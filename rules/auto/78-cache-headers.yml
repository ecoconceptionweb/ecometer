id: 78
bookId: 105
title: Add Expires or Cache-Control headers
slug: cache-headers
difficulty: easy
environmentalImpact: high
summary: | 
    The `Expires` and `Cache-Control` headers determine how long a browser should keep a resource in its cache. You should therefore use them, and configure them correctly for CSS style sheets, JavaScript scripts and images.  
    Ideally, these elements should be kept as long as possible so that the browser does not request them again from the server. This saves HTTP requests, bandwidth and CPU power server-side.
example: |
    Here is a configuration example for `Expires` and `Cache-Control` headers for the Apache web server:
    ```apache
    <IfModule mod_headers.c>
        <FilesMatch "\.(ico|jpe?g|png|gif|swf|css|gz)$">
            Header set Cache-Control "max-age=2592000, public"
        </FilesMatch>
        <FilesMatch "\.(html|htm)$">
            Header set Cache-Control "max-age=7200, public"
        </FilesMatch>
    </IfModule>
    ```
template: |
    {{.totalStaticResourcesWithCacheHeaders}} (probably static) resources out of {{.totalStaticResources}} have HTTP cache headers.
bookContext: Hosting
application: Caching
priority: 0
context: Hosting

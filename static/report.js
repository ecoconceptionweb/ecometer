function toggleClass(theElement, theClass) {
  if (theElement.classList) {
    theElement.classList.toggle(theClass);
  } else {
    var classes = theElement.className.split(' ');
    var existingIndex = classes.indexOf(theClass);
    if (existingIndex >= 0) {
      classes.splice(existingIndex, 1);
    } else {
      classes.push(theClass);
    }
    theElement.className = classes.join(' ');
  }
}

var toggleRulesList = document.querySelectorAll('.rules-list .card .toggle');
Array.prototype.forEach.call(toggleRulesList, function(el) {
  el.onclick = function() {
    toggleClass(el, 'active');
  };
});

var toggleMenu = document.querySelectorAll('header .toggle');
var navMenu = document.querySelectorAll('nav.menu');
toggleMenu[0].onclick = function() {
  toggleClass(navMenu[0], 'open');
};

package main

import (
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

func init() {
	expvar.NewInt("start_timestamp").Set(time.Now().Unix())
	expvar.Publish("timestamp", funcvar(func() string {
		return strconv.FormatInt(time.Now().Unix(), 10)
	}))

	expvar.Publish("jobs", funcvar(func() string {
		queued, running, _ := jobs.Size()
		return fmt.Sprintf(`{"queued": %d,"running": %d}`, queued, running)
	}))

	expvar.Publish("reports", funcvar(func() string {
		json, _ := store.Stats()
		return string(json)
	}))
}

type funcvar func() string

func (f funcvar) String() string {
	return f()
}

func recordStats(key string, h http.Handler) http.Handler {
	m := expvar.NewMap(key)
	responsesByStatus := new(expvar.Map).Init()
	m.Set("responses", responsesByStatus)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.Add("requests", 1)
		rw := &recordingResponseWriter{w: w}

		defer func(start time.Time) {
			if err := recover(); err != nil {
				m.Add("panics", 1)
				if rw.status == 0 {
					rw.status = http.StatusInternalServerError
				}
				// Simulate default http.Server behavior, without the stacktrace but with more request info
				if err != http.ErrAbortHandler {
					if logger := r.Context().Value(http.ServerContextKey).(*http.Server).ErrorLog; logger != nil {
						logger.Printf("http: panic serving %v %v to %v: %v\n", r.Method, r.RequestURI, r.RemoteAddr, err)
					} else {
						log.Printf("http: panic serving %v %v to %v: %v\n", r.Method, r.RequestURI, r.RemoteAddr, err)
					}
				}
			}
			responsesByStatus.Add("total", 1)
			responsesByStatus.Add(strconv.Itoa(rw.status), 1)
			m.Add("total_written", rw.written)
			m.AddFloat("total_duration", time.Since(start).Seconds())
		}(time.Now())

		h.ServeHTTP(rw, r)
	})
}

type recordingResponseWriter struct {
	w       http.ResponseWriter
	status  int
	written int64
}

func (rw *recordingResponseWriter) Header() http.Header {
	return rw.w.Header()
}

func (rw *recordingResponseWriter) WriteHeader(status int) {
	if rw.status == 0 {
		rw.status = status
	}
	rw.w.WriteHeader(status)
}

func (rw *recordingResponseWriter) Write(b []byte) (written int, err error) {
	if rw.status == 0 {
		rw.WriteHeader(http.StatusOK)
	}
	written, err = rw.w.Write(b)
	rw.written += int64(written)
	return
}

// ReadFrom is here to optimize copying from an *os.File regular file,
// because we know the default http.ResponseWriter is an io.ReaderFrom
// and http.FileServer takes advantage of it.
func (rw *recordingResponseWriter) ReadFrom(r io.Reader) (written int64, err error) {
	if rw.status == 0 {
		rw.WriteHeader(http.StatusOK)
	}
	written, err = io.Copy(rw.w, r)
	rw.written += written
	return
}

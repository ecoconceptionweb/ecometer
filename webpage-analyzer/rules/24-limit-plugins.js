import { OK, NOK } from '../status.js';
import { forEachFrame } from '../helpers';

// See https://gitlab.com/ecoconceptionweb/ecometer/issues/11
export const name = '24-limit-plugins';
export function check(page) {

  const plugins = [];

  forEachFrame(page, () => {
    // In a webpage, plugins are embedded with the <object /> or <embed /> tags
    plugins.push(...page.evaluate(() => {
      const elements = document.querySelectorAll('object,embed');
      return Array.from(elements).map(el => ({
        tag: el.tagName,
        src: el.tagName === 'EMBED' ? el.src : el.data,
        type: el.type,
      }));
    }));
  });

  return {
    id: 24,
    slug: 'limit-plugins',
    status: plugins.length === 0 ? OK : NOK,
    data: { plugins },
  };

}

export default { name, check };

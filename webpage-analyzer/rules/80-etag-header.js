import { OK, NOK, NA } from '../status.js';
import { isAsset, isResourceCompleted, debug } from '../helpers.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/12
export const name = '80-etag-header';
export function check(page, resources) {

  let totalStaticResources = 0;
  let totalStaticResourcesWithETagHeader = 0;
  let resourcesWithoutETag = [];

  Object.keys(resources)
    .forEach(resourceKey => {

      const resource = resources[resourceKey];

      if (!isResourceCompleted(resource)) return;
      if (!isAsset(resource.res.contentType)) return;

      debug(() => `The resource "${resource.res.url}" is considered static.`);

      totalStaticResources++;
      if (!hasValidETagHeader(resource.res))  {
        resourcesWithoutETag.push(resource.req.url);
        return;
      }

      debug(() => `The resource "${resource.res.url}" has a ETag header.`);
      totalStaticResourcesWithETagHeader++;

    })
  ;

  const ratio = totalStaticResourcesWithETagHeader/totalStaticResources*100;

  debug(() => `Static resources with Etag headers ratio ${ratio.toFixed(1)}%`);

  const status = totalStaticResources === 0 ? NA : ( ratio >= 95 ? OK : NOK );

  return {
    slug: 'etag-header',
    id: 80,
    status: status,
    data: { ratio, totalStaticResourcesWithETagHeader, totalStaticResources, resourcesWithoutETag },
  };

}

function hasValidETagHeader(response) {
  return response.headers.some(header => header.name === 'ETag');
}

export default { name, check };

import { forEachFrame } from '../helpers.js';
import { OK, NOK } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/32
export const name = '10-limit-css';
export function check(page) {

  const frames = [];

  forEachFrame(page, () => {
    frames.push({
      url: page.frameUrl,
      stylesheets: page.evaluate(function() {
        return Array.from(document.styleSheets).reduce((memo, sheet) => {

          const isPrint = sheet.media.includes('print');
          const isInlined = !sheet.href;

          // We ignore "print" and inlined CSS willingly
          if (isPrint || isInlined) return memo;

          memo.push(sheet.href);
          return memo;

        }, []);
      }),
    });
  });

  // We should limit ourselves to 2 external stylesheet per frame at most (1 common / 1 specific)
  const goal = 2;

  return {
    slug: 'limit-css',
    id: 10,
    status: frames.every(f => f.stylesheets.length <= goal) ? OK : NOK,
    data: { frames, goal },
  };

}

export default { name, check };

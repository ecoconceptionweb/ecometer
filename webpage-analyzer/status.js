export const OK = 'ok';
export const NOK = 'nok';
export const NA = 'na';
/* Reduce matrix:
 * s \ f | NA  | OK  | NOK
 *    NA | NA  | OK  | NOK
 *    OK | OK  | OK  | NOK
 *   NOK | NOK | NOK | NOK
 */
export function reduceStatus(acc, cur) {
  return (cur === NA || acc === NOK) ? acc : cur;
}
export default { OK, NOK, NA, reduceStatus };

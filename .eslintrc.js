/* eslint import/no-commonjs:off */
/* eslint-env node */

module.exports = {
  'root': true,
  'env': {
    'browser': true,
  },
  'extends': [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  'rules': {
    'array-bracket-spacing': [ 'error', 'always' ],
    'block-spacing': 'error',
    'brace-style': 'error',
    'comma-dangle': [ 'error', 'always-multiline' ],
    'comma-spacing': 'error',
    'curly':[ 'error', 'multi-line', 'consistent' ],
    'eol-last': 'error',
    'indent': [ 'error', 2 ],
    'keyword-spacing': 'error',
    'linebreak-style': 'error',
    'no-spaced-func': 'error',
    'no-trailing-spaces': 'error',
    'object-curly-spacing': [ 'error', 'always' ],
    'quotes': [ 'error', 'single', { 'avoidEscape': true } ],
    'semi': 'error',
    'semi-spacing': 'error',
    // import plugin
    'import/no-commonjs': 'error',
    'import/no-amd': 'error',
    'import/first': 'error',
    'import/no-duplicates': 'error',
    'import/newline-after-import': 'error',
    'import/prefer-default-export': 'error',
    'import/no-unassigned-import': 'error',
    'import/no-named-default': 'error',
  },
  'parserOptions': {
    'sourceType': 'module',
    'ecmaVersion': 2015,
  },
};
